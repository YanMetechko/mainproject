package by.minsk.itacademy.KHL.functions;

import by.minsk.itacademy.KHL.PlayerDetails.Player;

import java.util.Iterator;
import java.util.Scanner;

public class SearchEnquiries {
    public static void searchByName(KHLPlayers list) {
        Scanner scan = new Scanner(System.in);
        Iterator<Player> it = list.getLeaguePlayers().iterator();
        System.out.print("Type key name of a player you'd like to find.. ");
        String key = scan.nextLine();
        int i = 0;
        System.out.println("FOUND:");
        while (it.hasNext()) {
            Player plr = it.next();
            if ((plr.getName().getValue().indexOf(key)) != -1) {
                System.out.println(plr);
                i++;
            }
        }
        if (i == 0) System.out.println("nothing");
    }

    public static void searchByRole(KHLPlayers list) {
        Scanner scan = new Scanner(System.in);
        Iterator<Player> it = list.getLeaguePlayers().iterator();
        System.out.print("Type key role of the players you'd like to find.. ");
        String key = scan.nextLine();
        int i = 0;
        System.out.println("FOUND:");
        while (it.hasNext()) {
            Player plr = it.next();
            if ((plr.getRole().getValue().indexOf(key)) != -1) {
                System.out.println(plr);
                i++;
            }
        }
        if (i == 0) System.out.println("nothing");
    }
}

