package by.minsk.itacademy.KHL.functions;

import by.minsk.itacademy.KHL.PlayerDetails.Player;

import java.util.ArrayList;
import java.util.List;

public class KHLPlayers {

    private ArrayList<Player> leaguePlayers, defenders, centers, lWs, rWs;

    public KHLPlayers() {
        this.leaguePlayers = new ArrayList<>();
        this.defenders = new ArrayList<>();
        this.centers = new ArrayList<>();
        this.lWs = new ArrayList<>();
        this.rWs = new ArrayList<>();
    }

    public void addAPlayer(Player player) {
        leaguePlayers.add(player);
        if (player.getRole().getValue().equals("defender")) {
            defenders.add(player);
        } else if (player.getRole().getValue().equals("center")) {
            centers.add(player);
        } else if (player.getRole().getValue().equals("left wing")) {
            lWs.add(player);
        } else rWs.add(player);

    }

    public ArrayList<Player> getLeaguePlayers() {
        return leaguePlayers;
    }

    public void experienceCheck() {
        int ttl = 0;
        for (int i = 0; i < leaguePlayers.size(); i++) {
            ttl += leaguePlayers.get(i).getMatches().getValue();
        }
        int avrg = ttl / (leaguePlayers.size());
        System.out.println("The average quantity of games a player took part: " + avrg + ". In particular:");
        for (int i = 0; i < leaguePlayers.size(); i++) {


            if (leaguePlayers.get(i).getMatches().getValue() == avrg) {
                System.out.println(leaguePlayers.get(i).getName());
            }
        }
    }

    public void statisticsSummary() {
        System.out.println("Stats among defenders:");
        Stats.statsAnalysis(defenders);
        System.out.println("***");
        System.out.println("Stats among centers:");
        Stats.statsAnalysis(centers);
        System.out.println("***");
        System.out.println("Stats among left wingers:");
        Stats.statsAnalysis(lWs);
        System.out.println("***");
        System.out.println("Stats among right wingers:");
        Stats.statsAnalysis(rWs);
    }

    public void nationalityPtoportions() {
        List<String> natList = Stats.citizenshipStructure(leaguePlayers);
        List<String> tmList = Stats.teamshipStructure(leaguePlayers);
        for (String j : tmList) {
            int other = 0; int team_players =0;
            for (Player k : leaguePlayers) {
                if (k.getTeam().getValue().equals(j)) {
                    team_players++;
                    if (!(k.getNationality().getValue().equals("RUS") | k.getNationality().getValue().equals("BY")))
                        other++;
                }
            }
            double share = ((double) other / team_players)*100;
            System.out.print("Share of foreign players in " + j + " Hockey Club is ");
            System.out.printf( "%.2f", share);
            System.out.println(" %");
        }
        for (String i : natList) {
            int quantity = 0;
            for (Player k : leaguePlayers) {
                if (k.getNationality().getValue().equals(i)) quantity++;
            }
            double share = ((double) quantity / leaguePlayers.size())*100;
            System.out.print("Share of " + i + " players in the League is ");
            System.out.printf("%.2f", share);
            System.out.println(" %");
        }
    }
}
