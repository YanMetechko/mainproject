package by.minsk.itacademy.KHL.functions;

import by.minsk.itacademy.KHL.PlayerDetails.Player;

import java.util.*;

public class SortedByAgeSet {
    Comparator<Player> comparator = new Comparator<>(){
        @Override
        public int compare(Player a, Player b) {
            int i = -(a.getBirthday().getValue().compareTo(b.getBirthday().getValue()));
            if(i==0) i++;
            return i;
        }
    };
    private Set<Player> ageSortedSet = new TreeSet<Player>(comparator);

    public SortedByAgeSet(KHLPlayers currentRoster) {
        ArrayList<Player> list = currentRoster.getLeaguePlayers();
        for (Player i : list) {
            ageSortedSet.add(i);

        }
    }

    public void sortedRosterDisplay() {
        Iterator<Player> it = ageSortedSet.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }
}