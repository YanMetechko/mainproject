package by.minsk.itacademy.KHL.functions;

import by.minsk.itacademy.KHL.PlayerDetails.Player;

import java.util.*;

public class SortedBySeasonsSet {

    Comparator<Player> comparator = new Comparator<>() {
        @Override
        public int compare(Player o1, Player o2) {
            int i = -(o1.getSeasons().getValue().compareTo(o2.getSeasons().getValue()));
            if (i == 0) i++; //чтобы не выбрасывало всех кроме одного игроков с одинаковым количеством сезонов
            return i;
        }
    };


    private Set<Player> SeasonsSortedSet = new TreeSet<>(comparator);

    public SortedBySeasonsSet(KHLPlayers currentRoster) {
        ArrayList<Player> list = currentRoster.getLeaguePlayers();
        for (Player i : list) {
            SeasonsSortedSet.add(i);
        }
    }


    public void sortedRosterDisplay() {
        Iterator<Player> itt = SeasonsSortedSet.iterator();
        while (itt.hasNext()) {
            System.out.println(itt.next());
        }
    }
}