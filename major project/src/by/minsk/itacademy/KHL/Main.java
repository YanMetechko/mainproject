package by.minsk.itacademy.KHL;

import by.minsk.itacademy.KHL.PlayerDetails.Player;
import by.minsk.itacademy.KHL.functions.KHLPlayers;
import by.minsk.itacademy.KHL.functions.SearchEnquiries;
import by.minsk.itacademy.KHL.functions.SortedByAgeSet;
import by.minsk.itacademy.KHL.functions.SortedBySeasonsSet;

public class Main {

    public static void main(String[] args) {
        KHLPlayers roster = new KHLPlayers();
        roster.addAPlayer(new Player("Dmitry Vorobyev", "BY", 1, 8, 1986, 5, "SKA", "defender", 225, 75));
        roster.addAPlayer(new Player("Siarhei Manko", "BY", 31, 7, 1985, 3, "Avangard", "right wing", 83, 15));
        roster.addAPlayer(new Player("Eugen Stoljarchyk", "BY", 27, 5, 1987, 4, "CSKA", "center", 125, 25));
        roster.addAPlayer(new Player("Denis Hrabun", "BY", 21, 3, 1987, 6, "Ak Bars", "defender", 261, 79));
        roster.addAPlayer(new Player("Yan Miatsechko", "BY", 10, 9, 1986, 5, "Salavat Yulaev", "defender", 214, 61));
        roster.addAPlayer(new Player("Roman Abrosimov", "RUS", 31, 7, 1994, 2, "Ak Bars", "defender", 108, 8));
        roster.addAPlayer(new Player("Justin Azevedo", "CAN", 1, 4, 1988, 6, "Ak Bars", "defender", 275, 208));
        roster.addAPlayer(new Player("Philip Larsen", "DEN", 7, 12, 1989, 3, "Salavat Yulaev", "defender", 187, 113));
        roster.addAPlayer(new Player("Teemu Hartikainen", "FIN", 3, 5, 1990, 4, "Salavat Yulaev", "left wing", 287, 208));
        roster.addAPlayer(new Player("Alexei Yemelin", "RUS", 25, 4, 1986, 4, "Avangard", "defender", 195, 54));
        roster.addAPlayer(new Player("Pyotr Khokhryakov", "RUS", 16,1, 1990, 5, "Salavat Yulaev", "center", 372, 100));
        roster.addAPlayer(new Player("Andrei Stas", "BY", 18, 10, 1988, 6, "Avangard", "center", 482, 128));
        roster.addAPlayer(new Player("Nikolai Prokhorkin", "RUS",17 ,10 ,1993 ,5 , "SKA", "SKA",286,152 ));
        roster.addAPlayer(new Player("Anton Slepyshev", "RUS", 13, 5, 1994, 5, "CSKA", "right wing", 182, 58));
        roster.addAPlayer(new Player("Linden Vey", "CAN", 17, 7, 1991, 2, "CSKA", "left wing", 72, 69));
        roster.addAPlayer(new Player("Nikita Gusev", "RUS",8, 7, 1992, 7, "SKA", "right wing", 353, 280));
        roster.addAPlayer(new Player("Cody Franson", "CAN", 8, 8, 1987, 1, "Avangard", "defender", 19, 8));
        roster.addAPlayer(new Player("Jannik Hansen", "DEN", 15, 3, 1986, 1, "CSKA", "center", 18, 7));
        roster.addAPlayer(new Player("Jiri Sekac", "CZ", 10, 6, 1992, 4, "Ak Bars", "left wing", 231, 78));
        roster.addAPlayer(new Player("Patrik Hersley", "SWE", 23, 7, 1986, 4, "SKA", "defender", 208, 120));

       SortedByAgeSet sortedRoster = new SortedByAgeSet(roster);
        System.out.println("====================Sorted by Age=======================");
        sortedRoster.sortedRosterDisplay();

        System.out.println();
        System.out.println("====================Sorted by Seasons=======================");

        SortedBySeasonsSet reSortedRoster = new SortedBySeasonsSet(roster);
        reSortedRoster.sortedRosterDisplay();


        SearchEnquiries.searchByName(roster);
        SearchEnquiries.searchByRole(roster);
        roster.experienceCheck();
        roster.statisticsSummary();
        roster.nationalityPtoportions();
    }
}
