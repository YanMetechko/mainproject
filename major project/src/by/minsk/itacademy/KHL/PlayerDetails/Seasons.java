package by.minsk.itacademy.KHL.PlayerDetails;

public class Seasons extends PlayerDetails {


    public Seasons(Integer value) {
        super("seasons", value);
    }

    @Override
    public Integer getValue() {
        return (Integer) super.getValue();
    }
}
