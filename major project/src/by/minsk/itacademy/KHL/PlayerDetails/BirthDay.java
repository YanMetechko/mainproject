package by.minsk.itacademy.KHL.PlayerDetails;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class BirthDay extends PlayerDetails {


    public BirthDay(LocalDate value) {
        super("birthday", value);
    }

    @Override
    public LocalDate getValue() {
        return (LocalDate) super.getValue();
    }


    public String dateFormat() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.y");
        return this.getValue().format(formatter);
    }


}


