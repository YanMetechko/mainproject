package by.minsk.itacademy.KHL.PlayerDetails;

import by.minsk.itacademy.KHL.PlayerDetails.*;

import java.time.LocalDate;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Player {

    private Name name;
    private Nationality nationality;
    private BirthDay birthday;
    private Seasons seasons;
    private Team team;
    private Role role;
    private Matches matches;
    private Effectiveness effectiveness;
    private Scanner scan = new Scanner(System.in);
    private Scanner scan1 = new Scanner(System.in);
    private Scanner scan2 = new Scanner(System.in);
    private static Logger LOGGER = Logger.getLogger(Player.class.getName());


    /*public Player() {  // сделал более лаконичный конструктор (ниже) но этот  жалко удалять пока

        try {
            System.out.print("Enter Player's name:");
            this.name = new Name(scan.nextLine());
            System.out.print("Enter Player's nationality: ");
            this.nationality = new Nationality(scan.next());
            System.out.print("Enter Player's birthday. Day of month? ");
            int dayOfMonth = scan.nextInt();
            System.out.print("Month? ");
            int month = scan.nextInt();
            System.out.print("Year? ");
            int year = scan.nextInt();
            this.birthday = new BirthDay(LocalDate.of(year, month, dayOfMonth));
            System.out.print("Enter Player's KHL career number of seazons: ");
            this.seasons = new Seasons(scan1.nextInt());
            System.out.print("Enter Player's current KHL Team: ");
            this.team = new Team(scan2.nextLine());
            System.out.print("Enter Player's role: ");
            this.role = new Role(scan2.next());
            System.out.print("Enter Player's KHL career number of games: ");
            this.matches = new Matches(scan.nextInt());
            System.out.print("Enter Player's effectiveness: ");
            this.effectiveness = new Effectiveness(scan.nextInt());
            System.out.println();

        } catch (Exception exception) {
            LOGGER.log(Level.INFO,"Date is out of calendar bounds!", exception);
        }
    }*/

    public Player(String name, String nationality, int dayOfMonth, int month, int year, int seazons, String team, String role, int matches, int effectiveness) {

        try {
            this.name = new Name(name);
            this.nationality = new Nationality(nationality);
            this.birthday = new BirthDay(LocalDate.of(year, month, dayOfMonth));
            this.seasons = new Seasons(seazons);
            this.team = new Team(team);
            this.role = new Role(role);
            this.matches = new Matches(matches);
            this.effectiveness = new Effectiveness(effectiveness);

        } catch (Exception exception) {
            LOGGER.log(Level.INFO,"Date is out of calendar bounds!", exception);
        }
    }

    public BirthDay getBirthday() {
        return birthday;
    }

    public Seasons getSeasons() {
        return seasons;
    }

    public Name getName() {
        return name;
    }

    public Role getRole() {
        return role;
    }

    public Matches getMatches() {
        return matches;
    }

    public Effectiveness getEffectiveness() {
        return effectiveness;
    }

    public Nationality getNationality() {
        return nationality;
    }

    public Team getTeam() {
        return team;
    }

    @Override
    public String toString() {

        final char dm = (char) 34;
        StringBuilder card = new StringBuilder();
        try {
            card.append(dm).append(name.getFieldID()).append(dm + " : " + dm).append(name.getValue()).append(dm + ",\n");
            card.append(dm).append(nationality.getFieldID()).append(dm + " : " + dm).append(nationality.getValue()).append(dm + "\n");
            card.append(dm).append(birthday.getFieldID()).append(dm + " : " + dm).append(birthday.dateFormat()).append(dm + ",\n");
            card.append(dm).append(seasons.getFieldID()).append(dm + " : ").append(seasons.getValue()).append(",\n");
            card.append(dm).append(team.getFieldID()).append(dm + " : " + dm).append(team.getValue()).append(dm + ",\n");
            card.append(dm).append(role.getFieldID()).append(dm + " : " + dm).append(role.getValue()).append(dm + ",\n");
            card.append(dm).append(matches.getFieldID()).append(dm + " : ").append(matches.getValue()).append(",\n");
            card.append(dm).append(effectiveness.getFieldID()).append(dm + " : ").append(effectiveness.getValue() + "\n");
        } catch (Exception exception) {
            System.out.println("Date is out of possible range!");
        }
        return card.toString();
    }
}

