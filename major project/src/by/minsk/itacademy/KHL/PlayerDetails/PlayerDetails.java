package by.minsk.itacademy.KHL.PlayerDetails;

public class PlayerDetails {
    private final String FieldID;
    private Object Value;

    public String getFieldID() {
        return FieldID;
    }

    public Object getValue() {
        return Value;
    }

    public PlayerDetails(String fieldID, Object value) {
        FieldID = fieldID;
        this.Value = value;
    }
}
