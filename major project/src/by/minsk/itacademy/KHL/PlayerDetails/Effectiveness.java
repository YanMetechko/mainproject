package by.minsk.itacademy.KHL.PlayerDetails;

public class Effectiveness extends PlayerDetails {


    public Effectiveness(Integer value) {
        super("effectiveness", value);
    }

    @Override
    public Integer getValue() {
        return (Integer) super.getValue();
    }
}
